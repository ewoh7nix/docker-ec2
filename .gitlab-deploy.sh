#!/bin/bash
set -f

server=$DEPLOY_SERVER
ssh ubuntu@$server "sudo apt-get update && sudo apt-get install docker-ce -y && sudo systemctl restart docker && \
git clone $CI_REPOSITORY_URL && cd docker-ec2 && docker build -t stockbit-nginx:1.0 . && docker run -it -d -p 8000:80 stockbit-nginx:1.0"
