FROM alpine:3.14
LABEL version="1.0"
LABEL maintainer="ewoh7nix@gmail.com"
LABEL description="Base image with nginx"

RUN apk add --no-cache nginx
COPY hello.txt /var/www
COPY nginx-default.conf /etc/nginx/http.d/default.conf

EXPOSE 80
CMD ["nginx", "-g", "daemon off;"]
